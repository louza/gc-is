#!/usr/bin/env bash
mkdir -p external/sdsl-lite
git clone https://github.com/simongog/sdsl-lite.git external/sdsl-lite
./external/sdsl-lite/install.sh .
cd external/RePair
make
cd ../../
mkdir -p build
cd build
cmake .. -DCMAKE_BUILD_TYPE=RELEASE
make -j
make install
cd ..
cp external/RePair/repair  external/RePair/despair external/RePair/repair-memory  external/RePair/despair-memory bin/   
